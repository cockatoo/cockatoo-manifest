# Cockatoo Manifest

Repo manifest for checking out multiple repositories

To clone, install google repo and run this:

```
mkdir -p cockatoo
cd cockatoo
repo init -u git@gitlab.com:cockatoo/cockatoo-manifest.git
repo sync
```
